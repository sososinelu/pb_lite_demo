﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.PB_Access_2010DataSet = New WindowsApplication1.PB_Access_2010DataSet()
        Me.T0505BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T0505TableAdapter = New WindowsApplication1.PB_Access_2010DataSetTableAdapters.T0505TableAdapter()
        Me.TimeIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExecDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MattCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CommentDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ChangeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DurDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RateCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpRateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpCostDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BillNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BillDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TsselDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SummaryDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NotesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OffenceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClaimDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OutcomeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FixedFeeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GradeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HearingDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PersonsDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TSAYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Comment2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CaseStageLevelDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CategoryDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB_Access_2010DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T0505BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TimeIDDataGridViewTextBoxColumn, Me.ExecDataGridViewTextBoxColumn, Me.DateDataGridViewTextBoxColumn, Me.MattCodeDataGridViewTextBoxColumn, Me.ActionDataGridViewTextBoxColumn, Me.CommentDataGridViewTextBoxColumn, Me.ChangeDataGridViewTextBoxColumn, Me.DurDataGridViewTextBoxColumn, Me.QtyDataGridViewTextBoxColumn, Me.RateCodeDataGridViewTextBoxColumn, Me.RateDataGridViewTextBoxColumn, Me.UpRateDataGridViewTextBoxColumn, Me.CostDataGridViewTextBoxColumn, Me.UpCostDataGridViewTextBoxColumn, Me.BillNoDataGridViewTextBoxColumn, Me.BillDateDataGridViewTextBoxColumn, Me.StatusDataGridViewTextBoxColumn, Me.TsselDataGridViewTextBoxColumn, Me.SummaryDataGridViewTextBoxColumn, Me.NotesDataGridViewTextBoxColumn, Me.OffenceDataGridViewTextBoxColumn, Me.ClaimDataGridViewTextBoxColumn, Me.OutcomeDataGridViewTextBoxColumn, Me.FixedFeeDataGridViewTextBoxColumn, Me.GradeDataGridViewTextBoxColumn, Me.HearingDataGridViewTextBoxColumn, Me.PersonsDataGridViewTextBoxColumn, Me.TSAYDataGridViewTextBoxColumn, Me.Comment2DataGridViewTextBoxColumn, Me.CaseStageLevelDataGridViewTextBoxColumn, Me.CategoryDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.T0505BindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(12, 36)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1137, 574)
        Me.DataGridView1.TabIndex = 0
        '
        'PB_Access_2010DataSet
        '
        Me.PB_Access_2010DataSet.DataSetName = "PB_Access_2010DataSet"
        Me.PB_Access_2010DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'T0505BindingSource
        '
        Me.T0505BindingSource.DataMember = "T0505"
        Me.T0505BindingSource.DataSource = Me.PB_Access_2010DataSet
        '
        'T0505TableAdapter
        '
        Me.T0505TableAdapter.ClearBeforeFill = True
        '
        'TimeIDDataGridViewTextBoxColumn
        '
        Me.TimeIDDataGridViewTextBoxColumn.DataPropertyName = "TimeID"
        Me.TimeIDDataGridViewTextBoxColumn.HeaderText = "TimeID"
        Me.TimeIDDataGridViewTextBoxColumn.Name = "TimeIDDataGridViewTextBoxColumn"
        '
        'ExecDataGridViewTextBoxColumn
        '
        Me.ExecDataGridViewTextBoxColumn.DataPropertyName = "Exec"
        Me.ExecDataGridViewTextBoxColumn.HeaderText = "Exec"
        Me.ExecDataGridViewTextBoxColumn.Name = "ExecDataGridViewTextBoxColumn"
        '
        'DateDataGridViewTextBoxColumn
        '
        Me.DateDataGridViewTextBoxColumn.DataPropertyName = "Date"
        Me.DateDataGridViewTextBoxColumn.HeaderText = "Date"
        Me.DateDataGridViewTextBoxColumn.Name = "DateDataGridViewTextBoxColumn"
        '
        'MattCodeDataGridViewTextBoxColumn
        '
        Me.MattCodeDataGridViewTextBoxColumn.DataPropertyName = "MattCode"
        Me.MattCodeDataGridViewTextBoxColumn.HeaderText = "MattCode"
        Me.MattCodeDataGridViewTextBoxColumn.Name = "MattCodeDataGridViewTextBoxColumn"
        '
        'ActionDataGridViewTextBoxColumn
        '
        Me.ActionDataGridViewTextBoxColumn.DataPropertyName = "Action"
        Me.ActionDataGridViewTextBoxColumn.HeaderText = "Action"
        Me.ActionDataGridViewTextBoxColumn.Name = "ActionDataGridViewTextBoxColumn"
        '
        'CommentDataGridViewTextBoxColumn
        '
        Me.CommentDataGridViewTextBoxColumn.DataPropertyName = "Comment"
        Me.CommentDataGridViewTextBoxColumn.HeaderText = "Comment"
        Me.CommentDataGridViewTextBoxColumn.Name = "CommentDataGridViewTextBoxColumn"
        '
        'ChangeDataGridViewTextBoxColumn
        '
        Me.ChangeDataGridViewTextBoxColumn.DataPropertyName = "Change"
        Me.ChangeDataGridViewTextBoxColumn.HeaderText = "Change"
        Me.ChangeDataGridViewTextBoxColumn.Name = "ChangeDataGridViewTextBoxColumn"
        '
        'DurDataGridViewTextBoxColumn
        '
        Me.DurDataGridViewTextBoxColumn.DataPropertyName = "Dur"
        Me.DurDataGridViewTextBoxColumn.HeaderText = "Dur"
        Me.DurDataGridViewTextBoxColumn.Name = "DurDataGridViewTextBoxColumn"
        '
        'QtyDataGridViewTextBoxColumn
        '
        Me.QtyDataGridViewTextBoxColumn.DataPropertyName = "Qty"
        Me.QtyDataGridViewTextBoxColumn.HeaderText = "Qty"
        Me.QtyDataGridViewTextBoxColumn.Name = "QtyDataGridViewTextBoxColumn"
        '
        'RateCodeDataGridViewTextBoxColumn
        '
        Me.RateCodeDataGridViewTextBoxColumn.DataPropertyName = "RateCode"
        Me.RateCodeDataGridViewTextBoxColumn.HeaderText = "RateCode"
        Me.RateCodeDataGridViewTextBoxColumn.Name = "RateCodeDataGridViewTextBoxColumn"
        '
        'RateDataGridViewTextBoxColumn
        '
        Me.RateDataGridViewTextBoxColumn.DataPropertyName = "Rate"
        Me.RateDataGridViewTextBoxColumn.HeaderText = "Rate"
        Me.RateDataGridViewTextBoxColumn.Name = "RateDataGridViewTextBoxColumn"
        '
        'UpRateDataGridViewTextBoxColumn
        '
        Me.UpRateDataGridViewTextBoxColumn.DataPropertyName = "UpRate"
        Me.UpRateDataGridViewTextBoxColumn.HeaderText = "UpRate"
        Me.UpRateDataGridViewTextBoxColumn.Name = "UpRateDataGridViewTextBoxColumn"
        '
        'CostDataGridViewTextBoxColumn
        '
        Me.CostDataGridViewTextBoxColumn.DataPropertyName = "Cost"
        Me.CostDataGridViewTextBoxColumn.HeaderText = "Cost"
        Me.CostDataGridViewTextBoxColumn.Name = "CostDataGridViewTextBoxColumn"
        '
        'UpCostDataGridViewTextBoxColumn
        '
        Me.UpCostDataGridViewTextBoxColumn.DataPropertyName = "UpCost"
        Me.UpCostDataGridViewTextBoxColumn.HeaderText = "UpCost"
        Me.UpCostDataGridViewTextBoxColumn.Name = "UpCostDataGridViewTextBoxColumn"
        '
        'BillNoDataGridViewTextBoxColumn
        '
        Me.BillNoDataGridViewTextBoxColumn.DataPropertyName = "BillNo"
        Me.BillNoDataGridViewTextBoxColumn.HeaderText = "BillNo"
        Me.BillNoDataGridViewTextBoxColumn.Name = "BillNoDataGridViewTextBoxColumn"
        '
        'BillDateDataGridViewTextBoxColumn
        '
        Me.BillDateDataGridViewTextBoxColumn.DataPropertyName = "BillDate"
        Me.BillDateDataGridViewTextBoxColumn.HeaderText = "BillDate"
        Me.BillDateDataGridViewTextBoxColumn.Name = "BillDateDataGridViewTextBoxColumn"
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        '
        'TsselDataGridViewTextBoxColumn
        '
        Me.TsselDataGridViewTextBoxColumn.DataPropertyName = "tssel"
        Me.TsselDataGridViewTextBoxColumn.HeaderText = "tssel"
        Me.TsselDataGridViewTextBoxColumn.Name = "TsselDataGridViewTextBoxColumn"
        '
        'SummaryDataGridViewTextBoxColumn
        '
        Me.SummaryDataGridViewTextBoxColumn.DataPropertyName = "Summary"
        Me.SummaryDataGridViewTextBoxColumn.HeaderText = "Summary"
        Me.SummaryDataGridViewTextBoxColumn.Name = "SummaryDataGridViewTextBoxColumn"
        '
        'NotesDataGridViewTextBoxColumn
        '
        Me.NotesDataGridViewTextBoxColumn.DataPropertyName = "Notes"
        Me.NotesDataGridViewTextBoxColumn.HeaderText = "Notes"
        Me.NotesDataGridViewTextBoxColumn.Name = "NotesDataGridViewTextBoxColumn"
        '
        'OffenceDataGridViewTextBoxColumn
        '
        Me.OffenceDataGridViewTextBoxColumn.DataPropertyName = "Offence"
        Me.OffenceDataGridViewTextBoxColumn.HeaderText = "Offence"
        Me.OffenceDataGridViewTextBoxColumn.Name = "OffenceDataGridViewTextBoxColumn"
        '
        'ClaimDataGridViewTextBoxColumn
        '
        Me.ClaimDataGridViewTextBoxColumn.DataPropertyName = "Claim"
        Me.ClaimDataGridViewTextBoxColumn.HeaderText = "Claim"
        Me.ClaimDataGridViewTextBoxColumn.Name = "ClaimDataGridViewTextBoxColumn"
        '
        'OutcomeDataGridViewTextBoxColumn
        '
        Me.OutcomeDataGridViewTextBoxColumn.DataPropertyName = "Outcome"
        Me.OutcomeDataGridViewTextBoxColumn.HeaderText = "Outcome"
        Me.OutcomeDataGridViewTextBoxColumn.Name = "OutcomeDataGridViewTextBoxColumn"
        '
        'FixedFeeDataGridViewTextBoxColumn
        '
        Me.FixedFeeDataGridViewTextBoxColumn.DataPropertyName = "FixedFee"
        Me.FixedFeeDataGridViewTextBoxColumn.HeaderText = "FixedFee"
        Me.FixedFeeDataGridViewTextBoxColumn.Name = "FixedFeeDataGridViewTextBoxColumn"
        '
        'GradeDataGridViewTextBoxColumn
        '
        Me.GradeDataGridViewTextBoxColumn.DataPropertyName = "Grade"
        Me.GradeDataGridViewTextBoxColumn.HeaderText = "Grade"
        Me.GradeDataGridViewTextBoxColumn.Name = "GradeDataGridViewTextBoxColumn"
        '
        'HearingDataGridViewTextBoxColumn
        '
        Me.HearingDataGridViewTextBoxColumn.DataPropertyName = "Hearing"
        Me.HearingDataGridViewTextBoxColumn.HeaderText = "Hearing"
        Me.HearingDataGridViewTextBoxColumn.Name = "HearingDataGridViewTextBoxColumn"
        '
        'PersonsDataGridViewTextBoxColumn
        '
        Me.PersonsDataGridViewTextBoxColumn.DataPropertyName = "Persons"
        Me.PersonsDataGridViewTextBoxColumn.HeaderText = "Persons"
        Me.PersonsDataGridViewTextBoxColumn.Name = "PersonsDataGridViewTextBoxColumn"
        '
        'TSAYDataGridViewTextBoxColumn
        '
        Me.TSAYDataGridViewTextBoxColumn.DataPropertyName = "TSAY"
        Me.TSAYDataGridViewTextBoxColumn.HeaderText = "TSAY"
        Me.TSAYDataGridViewTextBoxColumn.Name = "TSAYDataGridViewTextBoxColumn"
        '
        'Comment2DataGridViewTextBoxColumn
        '
        Me.Comment2DataGridViewTextBoxColumn.DataPropertyName = "Comment2"
        Me.Comment2DataGridViewTextBoxColumn.HeaderText = "Comment2"
        Me.Comment2DataGridViewTextBoxColumn.Name = "Comment2DataGridViewTextBoxColumn"
        '
        'CaseStageLevelDataGridViewTextBoxColumn
        '
        Me.CaseStageLevelDataGridViewTextBoxColumn.DataPropertyName = "CaseStageLevel"
        Me.CaseStageLevelDataGridViewTextBoxColumn.HeaderText = "CaseStageLevel"
        Me.CaseStageLevelDataGridViewTextBoxColumn.Name = "CaseStageLevelDataGridViewTextBoxColumn"
        '
        'CategoryDataGridViewTextBoxColumn
        '
        Me.CategoryDataGridViewTextBoxColumn.DataPropertyName = "Category"
        Me.CategoryDataGridViewTextBoxColumn.HeaderText = "Category"
        Me.CategoryDataGridViewTextBoxColumn.Name = "CategoryDataGridViewTextBoxColumn"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1161, 622)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB_Access_2010DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T0505BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PB_Access_2010DataSet As WindowsApplication1.PB_Access_2010DataSet
    Friend WithEvents T0505BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T0505TableAdapter As WindowsApplication1.PB_Access_2010DataSetTableAdapters.T0505TableAdapter
    Friend WithEvents TimeIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ExecDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MattCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ActionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CommentDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ChangeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DurDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QtyDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RateCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UpRateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UpCostDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BillNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BillDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TsselDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SummaryDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NotesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OffenceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClaimDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OutcomeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FixedFeeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GradeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HearingDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PersonsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TSAYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Comment2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CaseStageLevelDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CategoryDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
